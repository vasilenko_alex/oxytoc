#Oxytoc site

### How to install and run

Let's just clone the repo by

```
git clone git@bitbucket.org:vasilenko_alex/oxytoc.git
```

1. Please install [Jekyll](http://jekyllrb.com/) first
2. Then please move to the project dir by `cd yourprojectdir`
3. After that make Jekyll serve you `jekyll serve`
4. Finally you'll be able to see the site on `http://0.0.0.0:4000/`
