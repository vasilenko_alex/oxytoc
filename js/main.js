/*
* HELITE - The Swiss Army Knife of Coming Soon Templates
* Build Date: June 2015
* Author: Madeon08
* Copyright (C) 2015 Madeon08
* This is a premium product available exclusively here : http://themeforest.net/user/Madeon08/portfolio
*/

/*  TABLE OF CONTENTS
    ---------------------------
    1. Opening effect
    2. Countdown
    3. Venobox Gallery
    4. Newsletter
    5. ScrollReveal
    6. Arrows / Go top link
    7. Twitter Feed
    8. Map, Structure & Design
*/

// When all the files were been loaded
//$(window).load(function() {
//
//    setTimeout(function() {
//        $(".loading").addClass("fadeOut").removeClass("");
//
//        var onMobile = false;
//
//            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) { onMobile = true; }
//            if( ( onMobile === false ) ) {
//
//                $("#header").addClass("fadeInLeft").removeClass("");
//
//            } else {
//
//                $("#header").addClass("fadeInUp").removeClass("");
//
//            }
//
//        $("#footer").addClass("fadeInLeft").removeClass("");
//
//    }, 600);
//
//    // FadeIn for the sections
//    setTimeout(function() {
//        $('section').each(function(i) {
//            (function(self) {
//                setTimeout(function() {
//                    $(self).addClass('fadeInUp').removeClass("opacity-0");
//                }, (i * 150) + 150);
//            })(this);
//        });
//
//    }, 1400);
//
//    // Display none for the loading
//    setTimeout(function() {
//        $(".loading").addClass("display-none").removeClass("");
//    }, 1800);
//
//});

$(document).ready(function($) {

    /* ------------------------------------- */
    /* 0. Random Portraits Order ........... */
    /* ------------------------------------- */

    var $container = $('#portraits');
    var $portraits = $('#portraits .dev-portrait');
    var $randomed = $portraits.sort(function () {
        return .5 - Math.random();
    });

    $container.empty().append($randomed).removeClass('hidden');

    /* ------------------------------------- */
    /* 2. Countdown ........................ */
    /* ------------------------------------- */

    // Set you end date just below
    // $('#countdown_dashboard').countDown({
    //     targetDate: {
    //         'day': 31,
    //         'month': 12,
    //         'year': 2016,
    //         'hour': 11,
    //         'min': 13,
    //         'sec': 0
    //     },
    //     omitWeeks: true
    // });

    /* ------------------------------------- */
    /* 3. Venobox Gallery .................. */
    /* ------------------------------------- */

     /* custom settings */
    // $('.venobox').venobox({
    //     border: '1px',             // default: '0'
    //     bgcolor: '#000',         // default: '#fff'
    //     titleattr: 'data-title',    // default: 'title'
    //     numeratio: true,            // default: false
    //     infinigall: true            // default: false
    // });

    /* ------------------------------------- */
    /* 4. Newsletter ....................... */
    /* ------------------------------------- */

    //$("#notifyMe").notifyMe();

    /* ------------------------------------- */
    /* 5. ScrollReveal ..................... */
    /* ------------------------------------- */

    //window.sr = new scrollReveal();

    /* ------------------------------------- */
    /* 6. Arrows / Go top link ............. */
    /* ------------------------------------- */

    // Hide go top link
    // $(".scroll-top").hide().css("bottom", "-100px");

    // // Show go top / Hide scroll
    // $(function () {

    //     $(window).scroll(function () {

    //         if ($(this).scrollTop() > 250) {
    //             $('.scroll-top').fadeIn().css("bottom", "0");
    //             $('.arrows-part').fadeOut();
    //         }

    //         else {

    //             $('.scroll-top').fadeOut().css("bottom", "-100px");
    //             $('.arrows-part').fadeIn();
    //         }
    //     });

    //     // Click go top
    //     $('a.go-top').click(function () {
    //         $('body,html').animate({
    //             scrollTop: 0
    //         }, 800);
    //         return false;
    //     });

    // });

    /* ------------------------------------- */
    /* 7. Twitter Feed ..................... */
    /* ------------------------------------- */
    //$('.twitter-block .tweet').twittie({
    //
    //    username: 'your_username',
    //    dateFormat: '%b. %d, %Y',
    //    template: '<div class="avatar-twitter">{{avatar}}</div><p class="screen-twitter">{{screen_name}}&nbsp;.&nbsp;<span class="date-twitter">{{date}}</span></p><p class="username-twitter">{{user_name}}</p><p class="tweet-twitter">{{tweet}}</p>',
    //    count: 10
    //
    //},
    //
    //function () {
    //var interval,
    //
    //    tweet_move = function () {
    //
    //        var item = $('.twitter-block .tweet ul').find('li:first');
    //
    //        if ($(window).width() <= 768) {
    //            item.animate( {marginTop: '-200px', 'opacity': '0', }, 500, function() {
    //                $(this).detach().appendTo('.twitter-block .tweet ul').removeAttr('style');
    //            });
    //        }
    //
    //        else {
    //            item.animate( {marginTop: '-150px', 'opacity': '0', }, 500, function() {
    //                $(this).detach().appendTo('.twitter-block .tweet ul').removeAttr('style');
    //            });
    //        }
    //
    //    };
    //
    //    interval = setInterval(tweet_move, 3500);
    //
    //    $('.twitter-block .tweet').hover(function () {
    //        clearInterval(interval);
    //    },
    //
    //    function () {
    //        interval = setInterval(tweet_move, 3500);
    //    });
    //
    //});

});

/* ------------------------------------- */
/* 8. Map, Structure & Design .......... */
/* ------------------------------------- */

// When the window has finished loading create our google map below
// google.maps.event.addDomListener(window, 'load', init);
// google.maps.event.addDomListener(window, 'resize', init);

// function init() {

//     // Basic options for a simple Google Map
//     // The latitude and longitude to center the map (always required)
//     var center = new google.maps.LatLng(47.846502, 35.128939);
//     // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
//     var mapOptions = {
//         // How zoomed in you want the map to start at (always required)
//         zoom: 13,
//         scrollwheel: false,
//         draggable: true,
//         center: center,

//         // How you would like to style the map.
//         // This is where you would paste any style found on Snazzy Maps.
//         styles: [{
//             "featureType": "administrative",
//             "elementType": "all",
//             "stylers": [{
//                 "visibility": "on"
//             }, {
//                 "lightness": 33
//             }]
//         }, {
//             "featureType": "landscape",
//             "elementType": "all",
//             "stylers": [{
//                 "color": "#f2e5d4"
//             }]
//         }, {
//             "featureType": "poi.park",
//             "elementType": "geometry",
//             "stylers": [{
//                 "color": "#c5dac6"
//             }]
//         }, {
//             "featureType": "poi.park",
//             "elementType": "labels",
//             "stylers": [{
//                 "visibility": "on"
//             }, {
//                 "lightness": 20
//             }]
//         }, {
//             "featureType": "road",
//             "elementType": "all",
//             "stylers": [{
//                 "lightness": 20
//             }]
//         }, {
//             "featureType": "road.highway",
//             "elementType": "geometry",
//             "stylers": [{
//                 "color": "#c5c6c6"
//             }]
//         }, {
//             "featureType": "road.arterial",
//             "elementType": "geometry",
//             "stylers": [{
//                 "color": "#e4d7c6"
//             }]
//         }, {
//             "featureType": "road.local",
//             "elementType": "geometry",
//             "stylers": [{
//                 "color": "#fbfaf7"
//             }]
//         }, {
//             "featureType": "water",
//             "elementType": "all",
//             "stylers": [{
//                 "visibility": "on"
//             }, {
//                 "color": "#acbcc9"
//             }]
//         }]
//     };

//     var map = new google.maps.Map(document.getElementById('map'), mapOptions, center);

//     var locations = [
//         ['<h6>Oxytoc - Game development team</h6><p><i class="fa fa-building"></i> This is your office!</p>', 47.846502, 35.128939, 1]
//     ];

//     var infowindow = new google.maps.InfoWindow();

//     var marker, i;

//     for (i = 0; i < locations.length; i++) {
//         marker = new google.maps.Marker({
//             position: new google.maps.LatLng(locations[i][1], locations[i][2]),
//             map: map
//         });

//         google.maps.event.addListener(marker, 'click', (function(marker, i) {
//             return function() {
//                 infowindow.setContent(locations[i][0]);
//                 infowindow.open(map, marker);
//             }
//         })(marker, i));
//     }

//     google.maps.event.addListener(marker, 'click', function() {
//         infowindow.open(map, marker);
//     });
// }